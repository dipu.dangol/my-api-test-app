export class Employee {
  id: number;
  fullName: String;
  address: String;
  email: String;
  phoneNumber: String;
}
