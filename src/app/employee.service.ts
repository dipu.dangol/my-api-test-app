import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from './employee';
import { Location } from '@angular/common';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private employeesUrl = 'http://localhost:8080/api/employees';  // URL to web api


  constructor(private http: HttpClient, private location: Location) { }


  getEmployees (): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeesUrl);
  }

  getEmployee(id: number): Observable<Employee> {
    const url = `${this.employeesUrl}/${id}`;
    return this.http.get<Employee>(url);
  }

  addEmployee (employee: Employee): void {
    this.http.post<Employee>(this.employeesUrl, employee, httpOptions)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

  updateEmployee (id: number, employee: Employee): Observable<Employee>  {
    const url = `${this.employeesUrl}/${id}`;
    return this.http.put<Employee>(url, employee, httpOptions);
  }

  deleteEmployee (employee: Employee | number): Observable<Employee> {
    console.log(2) ;
    const id = typeof employee === 'number' ? employee : employee.id;
    const url = `${this.employeesUrl}/${id}`;

    return this.http.delete<Employee>(url, httpOptions);
  }

}
